import React from "react"
import CartContextProvider from "./src/contexts/cart-context"
import { ThemeProvider } from "styled-components"
import Theme from "./src/styles/theme"

export const wrapRootElement = ({ element }) => (
  <ThemeProvider theme={Theme}>
    <CartContextProvider>{element}</CartContextProvider>
  </ThemeProvider>
)
