import React, { createContext, useReducer } from "react"
import { ContentfulProduct, CartProduct, CartContextInterface } from "../@types"

type CartContextProviderProps = {
  children: React.ReactNode
}

type ACTIONTYPE =
  | { type: "ADD_ITEM"; payload: CartProduct }
  | { type: "REMOVE_ITEM"; payload: CartProduct }
  | { type: "CLEAR_CART" }

export const CartContext = createContext<CartContextInterface>(
  {} as CartContextInterface
)

const getLocalCart = (): CartProduct[] => {
  if (typeof localStorage !== "undefined") {
    const cartItems = localStorage.getItem("cart") ?? "{}"
    return JSON.parse(cartItems)
  }
  return []
}

const persistCart = (cartItems: CartProduct[]): void => {
  if (typeof localStorage !== "undefined") {
    localStorage.setItem("cart", JSON.stringify(cartItems))
  }
}

const cartTotal = (): number => {
  return 0
}

const reducer = (state: { cartItems: CartProduct[] }, action: ACTIONTYPE) => {
  switch (action.type) {
    case "ADD_ITEM": {
      const item = state.cartItems.find(item => item.id === action.payload.id)
      if (item !== null && item !== undefined) {
        item.quantity += action.payload.quantity
      } else {
        state.cartItems.push({
          ...action.payload,
          quantity: action.payload.quantity,
        })
      }
      persistCart(state.cartItems)
      return { cartItems: [...state.cartItems] }
    }
    case "REMOVE_ITEM": {
      const item = state.cartItems.find(item => item.id === action.payload.id)
      if (item !== null && item !== undefined) {
        if (item.quantity > action.payload.quantity) {
          item.quantity -= action.payload.quantity
        } else {
          state.cartItems = state.cartItems.filter(
            item => !(item.id === action.payload.id)
          )
        }
      }
      persistCart(state.cartItems)
      return { cartItems: [...state.cartItems] }
    }
    case "CLEAR_CART": {
      persistCart([])
      return { cartItems: [] }
    }
    default: {
      return state
    }
  }
}

const CartContextProvider = ({ children }: CartContextProviderProps) => {
  const initialState = { cartItems: getLocalCart() }
  const [state, dispatch] = useReducer(reducer, initialState)

  const addItem = (product: ContentfulProduct, quantity: number): void => {
    let payload = { ...product, quantity }
    dispatch({ type: "ADD_ITEM", payload })
  }

  const removeItem = (product: ContentfulProduct, quantity: number): void => {
    let payload = { ...product, quantity }
    dispatch({ type: "REMOVE_ITEM", payload })
  }

  const clearCart = (): void => {
    dispatch({ type: "CLEAR_CART" })
  }

  const itemCount = (): number => cartTotal()

  const contextValues: CartContextInterface = {
    addItem,
    removeItem,
    clearCart,
    itemCount,
    ...state,
  }

  return (
    <CartContext.Provider value={contextValues}>
      {children}
    </CartContext.Provider>
  )
}

export default CartContextProvider
