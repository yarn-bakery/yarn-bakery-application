import { graphql, PageProps, Link } from "gatsby"
import * as React from "react"
import { Helmet } from "react-helmet"
import { ContentfulCollection, ContentfulProduct, Site } from "../@types"
import Layout from "../components/layout"
import ProductPreview from "../components/product-preview"
import styled from "styled-components"

type CollectionQueryProps = {
  site: Site
  allContentfulProduct: {
    edges: { node: ContentfulProduct }[]
  }
  contentfulCollection: ContentfulCollection
}

type CollectionPageProps = PageProps<CollectionQueryProps>

const BreadCrumb = styled.div`
  text-align: center;
  & > a {
    text-decoration: none;
  }

  & > span {
    padding: 0 0.6em;
  }
`

const Collection = ({ location, data }: CollectionPageProps) => {
  const products = data.allContentfulProduct.edges.map(product => product.node)
  const collection = data.contentfulCollection

  return (
    <Layout location={location}>
      <Helmet title={data.site.siteMetadata.title} />
      <div className="wrapper">
        <div className="row justify-content-center py-5">
          <BreadCrumb>
            <Link to="/producten">PRODUCTEN</Link> <span>{">"}</span>
            <Link to={`/collecties/${collection.fields.slug}`}>
              {collection.fields.slug.toUpperCase()}
            </Link>
          </BreadCrumb>
        </div>
        <h2 className="section-headline">Producten</h2>
        <div className="row row-cols-1 row-cols-sm-2 row-cols-lg-3 g-4 d-flex justify-content-around">
          {products.map(product => {
            return <ProductPreview product={product} />
          })}
        </div>
      </div>
    </Layout>
  )
}

export default Collection

export const query = graphql`
  query($collectionId: String!) {
    site {
      siteMetadata {
        title
        description
      }
    }
    contentfulCollection(id: { eq: $collectionId }) {
      id
      name
      preview {
        fluid(maxWidth: 400, maxHeight: 400) {
          ...GatsbyContentfulFluid
        }
      }
      fields {
        slug
      }
    }
    allContentfulProduct(
      filter: { collection: { id: { eq: $collectionId } } }
    ) {
      edges {
        node {
          id
          name
          fields {
            slug
          }
          collection {
            id
            name
            preview {
              fluid(maxWidth: 400, maxHeight: 400) {
                ...GatsbyContentfulFluid
              }
            }
            fields {
              slug
            }
          }
          description {
            description
          }
          preview {
            fluid(maxWidth: 400, maxHeight: 300, resizingBehavior: SCALE) {
              ...GatsbyContentfulFluid
            }
          }
        }
      }
    }
  }
`
