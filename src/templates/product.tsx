import React, { useContext, useState } from "react"
import { graphql, Link, PageProps } from "gatsby"
import Helmet from "react-helmet"
import Img, { FluidObject } from "gatsby-image"
import { Breadcrumb } from "gatsby-plugin-breadcrumb"
import "gatsby-plugin-breadcrumb/gatsby-plugin-breadcrumb.css"
import styled from "styled-components"
import { ContentfulProduct, Site } from "../@types"
import Layout from "../components/layout"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faShoppingBag } from "@fortawesome/free-solid-svg-icons"
import { CartContext } from "../contexts/cart-context"
import Quantity from "../components/quantity"
import ProductProperties from "../components/product-properties"

type ProductQueryProps = {
  site: Site
  contentfulProduct: ContentfulProduct
}

type ProductPageProps = PageProps<ProductQueryProps>

const Price = styled.h4`
  color: #acaaaa;
`

const Line = styled.div`
  height: 1px;
  margin: 2em 0;
  border-bottom: 1px solid #dddddd;
`

const Image = styled(Img)<{ fluid: FluidObject }>`
  max-width: 600px;
`

const OrderButton = styled.button`
  height: 60px;
  font-family: "Poiret One", "Poppins", Tahoma, Arial, Helvetica, sans-serif;
  font-size: 1.5em;
  text-align: center;
  border: none;
  color: ${props => props.theme.colors.light};
  background-color: ${props => props.theme.colors.secondary};
  &:hover {
    color: ${props => props.theme.colors.light};
    background-color: ${props => props.theme.colors.secondaryDark};
  }

  & > .orderbuttonicon {
    display: inline;
    font-family: "Poiret One", "Poppins", Tahoma, Arial, Helvetica, sans-serif;
    font-size: 1.2em;
    text-align: center;
  }

  & > span {
    display: inline;
    margin-left: 0.6em;
    font-size: 0.8em;
  }

  @media (max-width: ${props => props.theme.sizes.sm}) {
    & > .orderbuttonicon {
      font-size: 1em;
      margin: 0 0.5em;
    }

    & > span {
      display: none;
    }
  }
`

const BreadCrumb = styled.div`
  text-align: center;
  & > a {
    text-decoration: none;
  }

  & > span {
    padding: 0 0.6em;
  }
`

const Product = ({ location, data }: ProductPageProps) => {
  const product = data.contentfulProduct
  const collectionSlug = data.contentfulProduct.collection.name
    .toLowerCase()
    .replace(/\s/g, "-")
  const price = product.fields.stripePrice

  const { addItem } = useContext(CartContext)

  const [quantity, setQuantity] = useState(1)

  const priceStr = new Intl.NumberFormat("nl-NL", {
    style: "currency",
    currency: price.currency,
  }).format(price.unit_amount / 100)

  return (
    <Layout location={location}>
      <Helmet title={`${product.name}`} />
      <div className="container-fluid">
        <div className="row justify-content-center py-5">
          <BreadCrumb>
            <Link to="/producten">PRODUCTEN</Link> <span>{">"}</span>
            <Link to={`/collecties/${collectionSlug}`}>
              {product.collection.name.toUpperCase()}
            </Link>{" "}
            <span>{">"}</span>
            <Link
              to={`/producten/${product.fields.collectionSlug}/${product.fields.slug}`}
            >
              {product.name.toUpperCase()}
            </Link>
          </BreadCrumb>
        </div>
        <div className="row mx-5">
          <div className="col-md-12 col-lg-6">
            <Image alt={product.fields.slug} fluid={product.preview.fluid} />
          </div>
          <div className="col-md-12 col-lg-6">
            <h1>{product.name}</h1>
            <Price>{priceStr}</Price>
            <Line />
            <h5 className="m-0">Eigenschappen</h5>
            <ProductProperties product={product} />
            <div className="my-4">{product.description.description}</div>
            <Line />
            <div className="d-flex flex-row justify-content-between">
              <Quantity quantity={quantity} setQuantity={setQuantity} />
              <OrderButton
                type="button"
                className="btn btn-lg ms-3"
                onClick={() => addItem(product, quantity)}
              >
                <FontAwesomeIcon
                  icon={faShoppingBag}
                  className="orderbuttonicon"
                  size="1x"
                />
                <span>In winkelwagen</span>
              </OrderButton>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  )
}

export default Product

export const query = graphql`
  query($slug: String!) {
    contentfulProduct(fields: { slug: { eq: $slug } }) {
      name
      collection {
        name
      }
      description {
        description
      }
      preview {
        fluid(maxWidth: 800, maxHeight: 800, resizingBehavior: THUMB) {
          ...GatsbyContentfulFluid_tracedSVG
        }
      }
      fields {
        slug
        stripePrice {
          currency
          unit_amount
        }
      }
      isSuperwash
      meterage
      unitWeight
      weight
      fiberContent
      dye
    }
  }
`
