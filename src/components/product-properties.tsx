import * as React from "react"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import {
  faRulerHorizontal,
  faWeightHanging,
  faMapPin,
  faLeaf,
  faTshirt,
  faTint,
} from "@fortawesome/free-solid-svg-icons"
import styled from "styled-components"
import { ContentfulProduct } from "../@types"

type ProductPropertiesProps = {
  product: ContentfulProduct
}

const List = styled.ul`
  list-style-type: none;
`

const Icon = styled(FontAwesomeIcon)`
  display: inline-flex;
  margin-right: 1em;
`

const ProductProperties = ({ product }: ProductPropertiesProps) => {
  return (
    <List className="py-2 px-3">
      <li>
        <Icon icon={faTshirt} className="icon" fixedWidth />
        {product.fiberContent}
      </li>
      <li>
        <Icon icon={faRulerHorizontal} fixedWidth />
        {product.meterage} meter per 100 gram
      </li>
      <li>
        <Icon icon={faWeightHanging} fixedWidth />
        {product.unitWeight} gram
      </li>
      <li>
        <Icon icon={faMapPin} fixedWidth />
        {product.weight}
      </li>
      <li>
        <Icon icon={faLeaf} fixedWidth />
        {product.isSuperwash ? "superwash" : "geen superwash"}
      </li>
      <li>
        <Icon icon={faTint} fixedWidth />
        {product.dye} dye
      </li>
    </List>
  )
}

export default ProductProperties
