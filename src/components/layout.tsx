import * as React from "react"
import styled from "styled-components"
import { WindowLocation } from "@reach/router"
import Menu from "./menu"
import Navigation from "./navigation"
import Footer from "./footer"
import "../styles/global.scss"

type LayoutProps = {
  children?: React.ReactNode
  location: WindowLocation<WindowLocation["state"]>
}

const StyledMain = styled.main`
  background-color: ${props => props.theme.colors.light};
`

const Layout = ({ children }: LayoutProps) => {
  return (
    <div id="outer-container">
      <Menu />
      <Navigation />
      <StyledMain id="page-wrap">{children}</StyledMain>
      <Footer />
    </div>
  )
}

export default Layout
