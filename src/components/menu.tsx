import * as React from "react"
import { Link } from "gatsby"
import styled from "styled-components"

const OffCanvasMenu = styled.div`
  color: ${props => props.theme.colors.dark};
  background-color: ${props => props.theme.colors.light};
`

const MenuLink = styled(Link)`
  text-decoration: inherit;
  & > h3 {
    padding: 0.1em 0;
    margin-left: 1em;
  }
`

const Menu = () => {
  return (
    <OffCanvasMenu
      className="offcanvas offcanvas-end"
      tabIndex={-1}
      id="menu"
      aria-labelledby="menuLabel"
    >
      <div className="offcanvas-header">
        <h5 className="offcanvas-title" id="menuLabel"></h5>
        <button
          type="button"
          className="btn-close text-reset shadow-none"
          data-bs-dismiss="offcanvas"
          aria-label="Close"
        ></button>
      </div>
      <div className="offcanvas-body">
        <MenuLink to="/" data-bs-dismiss="offcanvas">
          <h3>Home</h3>
        </MenuLink>
        <MenuLink to="/producten" data-bs-dismiss="offcanvas">
          <h3>Producten</h3>
        </MenuLink>
      </div>
    </OffCanvasMenu>
  )
}

export default Menu
