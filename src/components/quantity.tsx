import * as React from "react"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faCircle, faMinus, faPlus } from "@fortawesome/free-solid-svg-icons"
import styled from "styled-components"

type QuantityProps = {
  quantity: number
  setQuantity: React.Dispatch<React.SetStateAction<number>>
}

const Label = styled.label`
  @media (max-width: ${props => props.theme.sizes.sm}) {
    display: none;
  }
`

const TextInput = styled.input`
  display: inline-block;
  text-align: center;
  font-size: 1, 4em;
  border-radius: 6px;
  border: 1px solid rgb(207, 207, 207);
  width: 60px;
  height: 60px;
`
const IconStack = styled.span`
  font-size: 0.6em;
`

const Quantity = ({ quantity, setQuantity }: QuantityProps) => {
  return (
    <div className="d-flex flex-row">
      <Label htmlFor="quantity" className="me-2 align-self-center">
        Aantal
      </Label>
      <TextInput
        type="text"
        id="quantity"
        name="Aantal"
        value={quantity}
        pattern="[1-9]+"
        className="me-1"
        onChange={e => setQuantity(+e.target.value)}
      />
      <span className="d-inline-flex flex-column justify-content-around">
        <IconStack
          role="button"
          className="fa-stack fa-2x"
          onClick={() => setQuantity(quantity + 1)}
        >
          <FontAwesomeIcon icon={faCircle} className="fa-stack-2x" />
          <FontAwesomeIcon
            icon={faPlus}
            className="fa-stack-1x"
            color="#fdfcfa"
          />
        </IconStack>
        <IconStack
          role="button"
          className="fa-stack fa-2x"
          onClick={() => quantity > 1 && setQuantity(quantity - 1)}
        >
          <FontAwesomeIcon icon={faCircle} className="fa-stack-2x" />
          <FontAwesomeIcon
            icon={faMinus}
            className="fa-stack-1x"
            color="#fdfcfa"
          />
        </IconStack>
      </span>
    </div>
  )
}

export default Quantity
