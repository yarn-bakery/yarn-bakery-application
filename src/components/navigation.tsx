import React, { useContext } from "react"
import { CartContext } from "../contexts/cart-context"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faBars, faShoppingBag } from "@fortawesome/free-solid-svg-icons"
import styled from "styled-components"

const Icon = styled(FontAwesomeIcon)`
  font-size: 2em;
  color: ${props => props.theme.colors.primary};
  margin: 0 0 0.3em 0;
`

const MenuIcon = styled(Icon)`
  margin-right: 0.2em;
  font-size: 1.8em;
  pointer-events: none;
`

const BrandText = styled.h1`
  padding: 0 0.2em;
  display: inline-block;
  font-family: "Poiret One";
  font-weight: 400;
  font-size: 2.2em;
  color: ${props => props.theme.colors.primary};
`
const Counter = styled.span`
  position: relative !important;
  top: -30px !important;
  right: 10px !important;
  font-size: 0.7em;
  background-color: rgb(252, 45, 97);
`

const Navigation = () => {
  const { itemCount } = useContext(CartContext)

  return (
    <nav className="navbar sticky-top bg-light">
      <div className="container-fluid">
        <BrandText>Yarn Bakery</BrandText>
        <div className="d-flex">
          <button className="btn px-1 shadow-none">
            <Icon role="button" icon={faShoppingBag} onClick={() => false} />
            <Counter className="badge rounded-pill">{itemCount()}</Counter>
          </button>

          <button
            className="btn px-1 shadow-none"
            type="button"
            data-bs-toggle="offcanvas"
            data-bs-target="#menu"
            aria-controls="menu"
          >
            <MenuIcon icon={faBars} />
          </button>
        </div>
      </div>
    </nav>
  )
}

export default Navigation
