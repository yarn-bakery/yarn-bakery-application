import * as React from "react"
import brand from "../../static/logo.svg"
import styled from "styled-components"

const StyledFooter = styled.footer`
  background: ${props => props.theme.colors.light};
  height: 500px;
`

const StyledBrand = styled.img`
  position: relative;
  top: 25%;
  height: 14em;
`

const Footer: React.FC = () => {
  return (
    <StyledFooter>
      <StyledBrand src={brand} alt="Brand" />
    </StyledFooter>
  )
}

export default Footer
