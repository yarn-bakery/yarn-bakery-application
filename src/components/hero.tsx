import * as React from "react"
import Img, { FluidObject } from "gatsby-image"
import styled from "styled-components"
import { ContentfulHighlight } from "../@types"

type HeroProps = {
  highlights: ContentfulHighlight[]
}

const PortraitImage = styled(Img)<{ fluid: FluidObject }>`
  display: none;

  @media (max-width: ${props => props.theme.sizes.md}) {
    display: block;
    height: 50vh;
    max-height: 600px;
  }
`

const LandscapeImage = styled(Img)<{ fluid: FluidObject }>`
  display: none;

  @media (min-width: ${props => props.theme.sizes.md}) {
    display: block;
    height: 40vw;
    max-height: 600px;
  }
`
const Hero = ({ highlights }: HeroProps) => {
  return (
    <div id="highlights" className="carousel slide" data-bs-ride="carousel">
      <div className="carousel-indicators">
        {highlights.map((highlight, index) => {
          return (
            <button
              key={highlight.id}
              type="button"
              data-bs-target="#highlights"
              data-bs-slide-to={index}
              className={index === 0 ? "active" : ""}
              aria-current={index === 0 ? true : false}
              aria-label={highlight.id}
            ></button>
          )
        })}
      </div>
      <div className="carousel-inner">
        {highlights.map((highlight, index) => {
          return (
            <div
              key={highlight.id}
              className={`carousel-item ${index === 0 ? "active" : ""}`}
            >
              <PortraitImage fluid={highlight.portraitImage.fluid} />
              <LandscapeImage fluid={highlight.landscapeImage.fluid} />
            </div>
          )
        })}
      </div>
      <button
        className="carousel-control-prev"
        type="button"
        data-bs-target="#highlights"
        data-bs-slide="prev"
      >
        <span className="carousel-control-prev-icon" aria-hidden="true"></span>
        <span className="visually-hidden">Previous</span>
      </button>
      <button
        className="carousel-control-next"
        type="button"
        data-bs-target="#highlights"
        data-bs-slide="next"
      >
        <span className="carousel-control-next-icon" aria-hidden="true"></span>
        <span className="visually-hidden">Next</span>
      </button>
    </div>
  )
}

export default Hero
