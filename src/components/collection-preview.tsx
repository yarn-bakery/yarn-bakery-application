import * as React from "react"
import Img, { FluidObject } from "gatsby-image"
import styled from "styled-components"
import { ContentfulCollection } from "../@types"
import { Link } from "gatsby"

type CollectionProps = {
  collection: ContentfulCollection
}

const Card = styled.div`
  border: none;
  max-width: 400px;
  background-color: ${props => props.theme.colors.light};
`

const CardOverlay = styled.div`
  display: flex;
  opacity: 0;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  color: ${props => props.theme.colors.light};
  background-color: ${props => props.theme.colors.secondary};
  transition: 0.3s;

  &:hover {
    opacity: 0.8;
    transition: 0.3s;
  }

  @media (max-width: ${props => props.theme.sizes.md}) {
    display: flex;
  }
`

const CardImage = styled(Img)<{ fluid: FluidObject }>``

const Button = styled(Link)`
  margin: 1em 0;
  display: block;
`

const CollectionPreview = ({ collection }: CollectionProps) => {
  return (
    <Card className="card">
      <CardImage
        className="card-img"
        fluid={collection.preview.fluid}
        alt={collection.name}
      />
      <CardOverlay className="card-img-overlay">
        <h2>{collection.name}</h2>
        <Button
          role="button"
          to={`/collecties/${collection.fields.slug}`}
          className="btn btn-lg btn-outline-light"
        >
          Bekijk
        </Button>
      </CardOverlay>
    </Card>
  )
}

export default CollectionPreview
