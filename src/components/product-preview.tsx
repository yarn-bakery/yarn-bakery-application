import * as React from "react"
import { Link } from "gatsby"
import Img from "gatsby-image"
import styled from "styled-components"
import { ContentfulProduct } from "../@types"

type ProductPreviewProps = {
  product: ContentfulProduct
}

const Column = styled.div`
  align-items: center;
  max-width: 400px;
`

const Card = styled.div`
  text-align: center;
  padding: 3em;
  border: none;
  border-radius: 20px;
`

const Button = styled(Link)`
  color: ${props => props.theme.colors.light};
  &:hover {
    color: ${props => props.theme.colors.light};
    background-color: ${props => props.theme.colors.secondaryDark};
  }
`

const ProductPreview = ({ product }: ProductPreviewProps) => {
  return (
    <Column className="col justify-content-center">
      <Card className="card p-3 bg-white">
        {product.preview && (
          <Img className="card-img-top" alt="" fluid={product.preview.fluid} />
        )}
        <div className="card-body pt-2">
          <h6 className="card-title">{product.name}</h6>
          <Button
            role="button"
            to={`/producten/${product.fields.slug}`}
            className="btn btn-secondary mt-4 w-100"
          >
            Bekijk
          </Button>
        </div>
      </Card>
    </Column>
  )
}

export default ProductPreview
