import { FluidObject } from "gatsby-image"

export interface ContentfulCollection {
  id: string
  name: string
  preview: {
    fluid: FluidObject
  }
  fields: {
    slug: string
  }
}

export interface ContentfulHighlight {
  id: string
  message: string
  portraitImage: {
    fluid: FluidObject
  }
  landscapeImage: {
    fluid: FluidObject
  }
}

export interface ContentfulProduct {
  id: string
  name: string
  collection: ContentfulCollection
  isHighlighted: boolean
  description: {
    description: string
  }
  preview: {
    fluid: FluidObject
  }
  fields: {
    slug: string
    stripePrice: {
      unit_amount: number
      currency: string
      id: string
    }
  }
  isSuperwash: boolean
  meterage: number
  unitWeight: string
  weight: number
  fiberContent: string
  dye: string
}

export interface CartProduct extends ContentfulProduct {
  quantity: number
}

export interface Site {
  siteMetadata: {
    title: string
    description: string
  }
}

export interface CartContextInterface {
  addItem: (product: ContentfulProduct, quantity: number) => void
  removeItem: (product: ContentfulProduct, quantity: number) => void
  clearCart: () => void
  itemCount: () => number
  cartItems: ContentfulProduct[]
}