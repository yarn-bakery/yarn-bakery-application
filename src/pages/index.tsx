import * as React from "react"
import PropTypes from "prop-types"
import { graphql, PageProps } from "gatsby"
import { Helmet } from "react-helmet"
import Hero from "../components/hero"
import Layout from "../components/layout"
import {
  ContentfulCollection,
  ContentfulHighlight,
  ContentfulProduct,
  Site,
} from "../@types"
import CollectionPreview from "../components/collection-preview"
import ProductPreview from "../components/product-preview"

type IndexQueryProps = {
  site: Site
  allContentfulHighlight: {
    edges: { node: ContentfulHighlight }[]
  }
  allContentfulCollection: {
    edges: { node: ContentfulCollection }[]
  }

  allContentfulProduct: {
    edges: { node: ContentfulProduct }[]
  }
}

type IndexPageProps = PageProps<IndexQueryProps>

const RootIndex = ({ location, data }: IndexPageProps) => {
  const siteTitle = data.site.siteMetadata.title

  const highlights = data.allContentfulHighlight.edges.map(
    highlight => highlight.node
  )
  const products = data.allContentfulProduct.edges.map(product => product.node)
  const collections = data.allContentfulCollection.edges.map(
    collection => collection.node
  )

  return (
    <Layout location={location}>
      <Helmet title={siteTitle} />
      <Hero highlights={highlights} />
      <div className="wrapper">
        <h2 className="section-headline">Uitgelicht</h2>
        <div className="row row-cols-1 row-cols-sm-2 row-cols-lg-3 g-4 d-flex justify-content-around">
          {products.map(product => (
            <ProductPreview key={product.id} product={product} />
          ))}
        </div>
        <h2 className="section-headline">Collecties</h2>
        <div className="row row-cols-1 row-cols-sm-2 row-cols-lg-3 g-4 d-flex justify-content-around">
          {collections.map(collection => (
            <CollectionPreview key={collection.id} collection={collection} />
          ))}
        </div>
      </div>
    </Layout>
  )
}

RootIndex.propTypes = {
  location: PropTypes.object,
  data: PropTypes.object,
}

export default RootIndex

export const pageQuery = graphql`
  query HomeQuery {
    site {
      siteMetadata {
        title
        description
      }
    }
    allContentfulHighlight {
      edges {
        node {
          id
          message
          portraitImage {
            fluid(
              maxWidth: 400
              maxHeight: 400
              resizingBehavior: CROP
              background: "rgb:000000"
            ) {
              ...GatsbyContentfulFluid
            }
          }
          landscapeImage {
            fluid(
              maxWidth: 2000
              resizingBehavior: PAD
              background: "rgb:000000"
            ) {
              ...GatsbyContentfulFluid
            }
          }
        }
      }
    }
    allContentfulProduct(filter: { isHighlighted: { eq: true } }, limit: 4) {
      edges {
        node {
          id
          name
          dye
          isHighlighted
          description {
            description
          }
          preview {
            fluid(maxWidth: 400, maxHeight: 400, resizingBehavior: THUMB) {
              ...GatsbyContentfulFluid
            }
          }
          fields {
            slug
            stripePrice {
              unit_amount
              currency
              id
            }
          }
        }
      }
    }
    allContentfulCollection {
      edges {
        node {
          id
          name
          fields {
            slug
          }
          preview {
            fluid(maxWidth: 400, maxHeight: 400) {
              ...GatsbyContentfulFluid
            }
          }
        }
      }
    }
  }
`
