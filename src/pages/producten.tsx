import * as React from "react"
import { graphql, Link, PageProps } from "gatsby"
import { Helmet } from "react-helmet"
import styled from "styled-components"
import { Breadcrumb } from "gatsby-plugin-breadcrumb"
import Layout from "../components/layout"
import { Site } from "../@types"

type PageQueryProps = {
  site: Site
}

type ProductsPageProps = PageProps<PageQueryProps>

const LinkButton = styled(Link)`
  text-decoration: inherit;
`
const BreadCrumb = styled.div`
  text-align: center;
  & > a {
    text-decoration: none;
  }

  & > span {
    padding: 0 0.6em;
  }
`

const ProductsPage = ({ location, data }: ProductsPageProps) => {
  return (
    <Layout location={location}>
      <Helmet title={data.site.siteMetadata.title} />
      <div className="wrapper">
        <div className="row justify-content-center py-5">
          <BreadCrumb>
            <Link to="/producten">PRODUCTEN</Link>
          </BreadCrumb>
        </div>
        <div className="container">
          <div className="row">
            <div className="col">
              <h1>Acid Dye</h1>
              <LinkButton
                role="button"
                className="btn btn-primary"
                to="/producten/acid-dye"
              >
                Bekijken
              </LinkButton>
            </div>
            <div className="col">
              <h1>Natural Dye</h1>
              <LinkButton
                role="button"
                className="btn btn-primary"
                to="/producten/natural-dye"
              >
                Bekijken
              </LinkButton>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  )
}

export default ProductsPage

export const pageQuery = graphql`
  {
    site {
      siteMetadata {
        title
        description
      }
    }
  }
`
