const Theme = {
  colors: {
    primary: "#456965",
    primaryDark: "#375451",
    secondary: "#b4858d",
    secondaryDark: "#9b5f69",
    light: "#fdfcfa",
    dark: "#404040",
    background: "#F6F3E3",
    backgroundDark: "#172927",
  },
  sizes: {
    xs: "0px",
    sm: "576px",
    md: "768px",
    lg: "992px",
    xl: "1200px",
    xxl: "1400px",
  },
}

export default Theme