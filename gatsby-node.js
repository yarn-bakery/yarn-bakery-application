const Promise = require("bluebird")
const path = require("path")

exports.onCreateNode = ({ node, actions }) => {
  const { createNodeField } = actions

  if (node.internal.type === "ContentfulProduct") {
    createNodeField({
      node,
      name: "slug",
      value: node.name.toLowerCase().replace(/\s/g, "-"),
    })

    createNodeField({
      node,
      name: "stripePrice___NODE",
      value: node.stripeId,
    })
  } else if (node.internal.type === "ContentfulCollection") {
    createNodeField({
      node,
      name: "slug",
      value: node.name.toLowerCase().replace(/\s/g, "-"),
    })
  }
}

exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions

  return new Promise((resolve, reject) => {
    const productTemplate = path.resolve("./src/templates/product.tsx")
    const collectionTemplate = path.resolve("./src/templates/collection.tsx")
    resolve(
      graphql(
        `
          {
            allContentfulProduct {
              edges {
                node {
                  dye
                  fields {
                    slug
                  }
                }
              }
            }
            allContentfulCollection {
              edges {
                node {
                  id
                  name
                  fields {
                    slug
                  }
                }
              }
            }
          }
        `
      ).then(result => {
        if (result.errors) {
          console.log(result.errors)
          reject(result.errors)
        }
        result.data.allContentfulProduct.edges.map(({ node }) => {
          createPage({
            path: `/producten/${node.fields.slug}`,
            component: productTemplate,
            context: {
              slug: node.fields.slug,
            },
          })
        })

        result.data.allContentfulCollection.edges.map(({ node }) => {
          console.log(node)
          createPage({
            path: `/collecties/${node.fields.slug}`,
            component: collectionTemplate,
            context: {
              slug: node.fields.slug,
              collectionId: node.id,
            },
          })
        })
      })
    )
  })
}
