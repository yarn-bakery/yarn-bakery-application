require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
})

// Configure Contentful

const contentfulConfig = {
  spaceId: process.env.CONTENTFUL_SPACE_ID,
  accessToken:
    process.env.CONTENTFUL_ACCESS_TOKEN ||
    process.env.CONTENTFUL_DELIVERY_TOKEN,
}

if (process.env.CONTENTFUL_HOST) {
  contentfulConfig.host = process.env.CONTENTFUL_HOST
  contentfulConfig.accessToken = process.env.CONTENTFUL_PREVIEW_ACCESS_TOKEN
}

const { spaceId, accessToken } = contentfulConfig

if (!spaceId || !accessToken) {
  throw new Error(
    "Contentful spaceId and the access token need to be provided."
  )
}

// Configure Stripe

const stripeConfig = {
  publishableKey: process.env.STRIPE_PUBLISHABLE_KEY,
  secretKey: process.env.STRIPE_SECRET_KEY,
}

const { publishableKey, secretKey } = stripeConfig

if (!publishableKey || !secretKey) {
  throw new Error(
    "Stripe publishable key and the secret key need to be provided."
  )
}

module.exports = {
  siteMetadata: {
    title: "Yarn Bakery",
  },
  pathPrefix: "/yarn-bakery",
  plugins: [
    "gatsby-transformer-remark",
    "gatsby-transformer-sharp",
    "gatsby-plugin-react-helmet",
    "gatsby-plugin-sharp",
    "gatsby-plugin-styled-components",
    "gatsby-plugin-sass",
    {
      resolve: "gatsby-source-stripe",
      options: {
        objects: ["Price"],
        secretKey: secretKey,
        downloadFiles: true,
      },
    },
    {
      resolve: "gatsby-source-contentful",
      options: contentfulConfig,
    },
    {
      resolve: `gatsby-plugin-breadcrumb`,
      options: {
        usePathPrefix: "/yarn-bakery",
      },
    },
  ],
}
